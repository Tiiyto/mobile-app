package com.example.thdf

import android.app.Application
import com.example.thdf.model.Cache
import com.example.thdf.model.Chasse
import com.example.thdf.model.Question

class CurrentChasseApplication: Application() {
    companion object
    {
        var currentChasse: Chasse? =  null
    }

    override fun onCreate() {
        super.onCreate()
        currentChasse = null
    }
}