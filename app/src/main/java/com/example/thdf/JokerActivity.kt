package com.example.thdf

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.room.Room
import com.bumptech.glide.Glide
import com.example.thdf.dao.CurrentChasseDao
import com.example.thdf.database.RoomDatabase
import com.example.thdf.model.CurrentChasse
import com.example.thdf.presenter.ChasseApiPresenter
import com.example.thdf.presenter.CurrentChassePresenter
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class JokerActivity : AppCompatActivity(), CurrentChassePresenter.CurrentChassePresenterView {
    val presenter: CurrentChassePresenter = CurrentChassePresenter(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_joker)
        presenter.getCurrentChasse(this)
        val close: Button = findViewById(R.id.closeJoker)
        close.setOnClickListener {
            finish()
        }
    }

    override fun CurrentChasse(currentChasse: CurrentChasse) {
        Glide.with(this)
            .load(currentChasse.questions?.get(currentChasse.currentQuestion!!)!!.QuestionIndice)
            .into(findViewById(R.id.imageJoker))    }

    override fun CurrentChasseError(throwable: Throwable) {
        Toast.makeText(this, throwable.message, Toast.LENGTH_SHORT).show()
    }
}