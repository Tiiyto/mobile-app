package com.example.thdf

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide

class PlusFragment : Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val view : View = inflater.inflate(R.layout.fragment_plus, container, false)

        val logo = view.findViewById<ImageView>(R.id.big_logo)
        Glide.with(this).load(R.drawable.logo).into(logo)

        val politique = view.findViewById<TextView>(R.id.politique)
        politique.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://tresorsdeshautsdefrance.com/politique-de-confidentialite/")))
        }

        val mBtnTuto = view.findViewById<Button>(R.id.btn_tuto)
        mBtnTuto.setOnClickListener{
            resetSharedPref()
            Toast.makeText(context, getString(R.string.revoir_tuto_toast), Toast.LENGTH_SHORT).show()
        }

        /*
        * Bouton voir les chasses téléchargées
        * */
        val btnTrackDl = view.findViewById<Button>(R.id.btn_trackDl)
        btnTrackDl.setOnClickListener{
            val intent = Intent(context, TrackDlActivity::class.java)
            startActivity(intent)
        }

        return view
    }

    private fun resetSharedPref(){

        val sp : SharedPreferences? = context?.getSharedPreferences("firstUse", Context.MODE_PRIVATE)

        val firstList = sp?.getBoolean("firstList", true)
        val editor : SharedPreferences.Editor? = sp?.edit()
        if(!firstList!!){
            editor?.putBoolean("firstList", true)?.apply()
        }

        val firstMap = sp.getBoolean("firstMap",true)
        if(!firstMap){
            editor?.putBoolean("firstMap", true)?.apply()
        }
        val firstToChasse = sp.getBoolean("firstToChasse", true)
       if(!firstToChasse){
           editor?.putBoolean("firstToChasse", true)?.apply()
       }

    }


}