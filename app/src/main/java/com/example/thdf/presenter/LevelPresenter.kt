package com.example.thdf.presenter

import com.example.thdf.model.Chasse
import com.example.thdf.service.RetrofitBuilder
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class LevelPresenter(private val view: levelPresenterView) {

    fun getChassesByLvl(lvlChasse: Int) {
        val chasseService = RetrofitBuilder().getChasseService()

        chasseService.getListByLvl(lvlChasse)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({ listChasse ->
                view.listChasseLoaded(listChasse)
            }, { error ->
                view.listChasseError(error)
            })
    }

    interface levelPresenterView {
        fun listChasseLoaded(listChasse: List<Chasse>)
        fun listChasseError(throwable: Throwable)
    }

}