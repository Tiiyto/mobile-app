package com.example.thdf.presenter

import com.example.thdf.model.User
import com.example.thdf.model.UserSignUp
import com.example.thdf.service.RetrofitBuilder
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class SignUpPresenter(private val view: SignUpPresenterView) {
    fun signup(user: UserSignUp)
    {
        val userService = RetrofitBuilder().getUserService()
        userService.signup(user)
                .subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({ user ->
                    view.signUpUser(user)
                }, { error ->
                    view.signUpError(error)
                })
    }
    interface SignUpPresenterView
    {
        fun signUpUser(user: User)
        fun signUpError(throwable: Throwable)
    }
}