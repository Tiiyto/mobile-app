package com.example.thdf.presenter

import com.example.thdf.model.User
import com.example.thdf.service.RetrofitBuilder
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class LoginPresenter(private val view: LoginPresenterView) {
    fun login(user: User)
    {
        val userService = RetrofitBuilder().getUserService()
        userService.login(user)
                .subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({ compte ->
                    view.loginUser(compte)
                },{ error ->
                    view.loginError(error)
                })
    }
    interface LoginPresenterView
    {
        fun loginUser(user: User)
        fun loginError(throwable: Throwable)
    }
}