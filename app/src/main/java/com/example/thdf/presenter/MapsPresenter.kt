package com.example.thdf.presenter

import android.content.Context
import androidx.room.Room
import com.example.thdf.dao.ChasseDao
import com.example.thdf.dao.CurrentChasseDao
import com.example.thdf.database.RoomDatabase
import com.example.thdf.model.Chasse
import com.example.thdf.model.CurrentChasse
import com.example.thdf.service.RetrofitBuilder
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import java.io.File

class MapsPresenter(private val view: MapsPresenterPresenterView) {

    fun getCurrentChasse(context: Context) {
        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val currentDao: CurrentChasseDao = database.currentChasseDao()
        currentDao.get()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ currentChasse ->
                    view.CurrentChasse(currentChasse)
                }, { error ->
                    view.CurrentChasseError(error)
                })
    }
    fun deleteCurrentChasse(context: Context, currentChasse: CurrentChasse)
    {
        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val currentDao: CurrentChasseDao = database.currentChasseDao()
        currentDao.get()
                .map { currentchasse ->
                    if(!currentChasse.fromDB)
                    {
                        currentchasse.questions?.forEach { question ->
                            val file = File(question.QuestionIndice)
                            file.delete()
                        }
                    }
                    currentchasse
                }
                .flatMapCompletable { currentChasse ->
                    currentDao.delete(currentChasse)
                }
                .subscribe()
    }

    fun getListChasse()
    {
        val chasseService = RetrofitBuilder().getChasseService()
        chasseService.listChasse()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ chasses ->
                    view.listChasse(chasses)
                },{ error ->
                    view.listChasseError(error)
                })

    }

    fun getListChasseDb(context: Context)
    {
        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val chasseDao: ChasseDao = database.chasseDao()
        chasseDao.getAllTracks()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ chasses -> 
                    view.listChasseDb(chasses)
                },{ error -> 
                    view.listChasseDbError(error)
                })
    }
    

    interface MapsPresenterPresenterView {
        fun CurrentChasse(currentChasse: CurrentChasse)
        fun CurrentChasseError(throwable: Throwable)
        
        fun listChasse(listChasse: List<Chasse>)
        fun listChasseError(throwable: Throwable)

        fun listChasseDb(listChasse: List<Chasse>)
        fun listChasseDbError(throwable: Throwable)
    }
}