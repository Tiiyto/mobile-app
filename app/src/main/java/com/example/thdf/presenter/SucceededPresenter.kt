package com.example.thdf.presenter

import android.content.Context
import androidx.room.Room
import com.example.thdf.dao.CurrentChasseDao
import com.example.thdf.database.RoomDatabase
import com.example.thdf.model.CurrentChasse
import com.example.thdf.model.PostChasseDone
import com.example.thdf.service.RetrofitBuilder
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class SucceededPresenter(private val view: SucceededPresenterView) {
    fun getCurrentChasse(context: Context) {
        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val currentDao: CurrentChasseDao = database.currentChasseDao()
        currentDao.get()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ currentChasse ->
                    view.CurrentChasse(currentChasse)
                }, { error ->
                    view.CurrentChasseError(error)
                })
    }

    fun postChasseDone(info: PostChasseDone?)
    {
        val chasseService = RetrofitBuilder().getChasseService()
        chasseService.postChasseDone(info)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                }, { error ->
                    view.postChasseDoneError(error)
                })
    }

    interface SucceededPresenterView {
        fun CurrentChasse(currentChasse: CurrentChasse)
        fun CurrentChasseError(throwable: Throwable)

        fun postChasseDoneError(throwable: Throwable)
    }
}