package com.example.thdf.presenter

import com.example.thdf.model.Chasse
import com.example.thdf.service.ChasseService
import com.example.thdf.service.RetrofitBuilder
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ChasseApiPresenter(private val view: ChasseApiPresenterView) {

    fun getChassesApi ()
    {
        val chasseService = RetrofitBuilder().getChasseService()
        chasseService.listChasse()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ chasseList ->
                view.listChasseApi(chasseList)
            },{ error ->
                view.listChasseError(error)
            })
    }
    interface ChasseApiPresenterView
    {
        fun listChasseError(throwable: Throwable)
        fun listChasseApi(listChasse: List<Chasse>)
    }
}