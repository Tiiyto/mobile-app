package com.example.thdf.presenter

import com.example.thdf.model.CompletedChasse
import com.example.thdf.service.RetrofitBuilder
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class ChasseDonePresenter(private val view: ChasseDonePresenterView) {
    fun getChassesDone(id_user: Int?)
    {
        val chasseService = RetrofitBuilder().getChasseService()
        chasseService.getListChasseDone(id_user)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({ listChasseCompleted ->
                    view.listChasseDone(listChasseCompleted)
                },{ error ->
                    view.listChasseDoneError(error)
                })
    }
    interface ChasseDonePresenterView
    {
        fun listChasseDone(listChasseDone: List<CompletedChasse>)
        fun listChasseDoneError(throwable: Throwable)
    }
}
