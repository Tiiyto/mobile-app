package com.example.thdf.presenter

import android.content.Context
import android.widget.Toast
import androidx.room.Room
import com.example.thdf.dao.CurrentChasseDao
import com.example.thdf.database.RoomDatabase
import com.example.thdf.model.Cache
import com.example.thdf.model.CurrentChasse
import com.example.thdf.model.User
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class QuestionPresenter(private val view: QuestionPresenterView) {
    fun decrementeJoker(context: Context)
    {
        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val currentDao = database.currentChasseDao()
        currentDao.decrementeJoker()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
    }
    fun getCurrentChasse(context: Context)
    {
        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val currentDao = database.currentChasseDao()
        currentDao.get()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ currentChasse ->
                    view.CurrentChasse(currentChasse)
                }, { error->
                    view.CurrentChasseError(error)
                })
    }
    fun incrementCurrentQuestion (context: Context, currentChasse: CurrentChasse)
    {
        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val currentDao: CurrentChasseDao = database.currentChasseDao()
        currentChasse.currentQuestion = currentChasse.currentQuestion?.plus(1)
        currentDao.incrementCurrentQuestion()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
    }
    fun activeJoker(context: Context) {
        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val currentDao: CurrentChasseDao = database.currentChasseDao()
        currentDao.activeJoker()
                .subscribeOn(Schedulers.io())
                .subscribe()
    }
    fun desactiveJoker(context: Context) {
        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val currentDao: CurrentChasseDao = database.currentChasseDao()
        currentDao.desactiveJoker()
                .subscribeOn(Schedulers.io())
                .subscribe()
    }
    fun getJokerActivate(context: Context) {
        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val currentDao: CurrentChasseDao = database.currentChasseDao()
        currentDao.getJokerActivated()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ state ->
                    view.getJokerActivated(state)
                }, { error->
                    view.getJokerActivatedError(error)
                })
    }
    interface QuestionPresenterView
    {
        fun CurrentChasse(currentChasse: CurrentChasse)
        fun CurrentChasseError(throwable: Throwable)
        fun getJokerActivated(state: Boolean)
        fun getJokerActivatedError(throwable: Throwable)
    }
}