package com.example.thdf.presenter

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.View
import androidx.room.Room
import com.example.thdf.CurrentChasseApplication
import com.example.thdf.R
import com.example.thdf.dao.CacheDao
import com.example.thdf.dao.ChasseDao
import com.example.thdf.database.RoomDatabase
import com.example.thdf.model.Chasse
import com.example.thdf.service.ChasseService
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File

class ChassePresenter(private val view: chassePresenterView) {

    fun getChasses(context: Context) {

        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val chasseDao = database.chasseDao()

        chasseDao.getAllTracks()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ listChasse->
                    view.listChasseDl(listChasse)
                },{ error ->
                    view.listChasseError(error)
                })
    }
    fun deleteListChasse(context: Context, chasses : ArrayList<Chasse>)
    {
        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val questionDao = database.questionDao()
        val chasseDao : ChasseDao = database.chasseDao()
        val cacheDao : CacheDao = database.cacheDao()
        val ids = chasses.map { it.TrackID }
        Completable.complete()
                .andThen(chasseDao.deleteItemByIds(ids))
                .andThen(cacheDao.deleteItemByIds(ids))
                .andThen(questionDao.deleteItemByIds(ids))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{ view.deleted()}
        questionDao.getQuestionByIds(ids)
                .map { questions ->
                    questions.forEach { question ->
                        val file = File(question.QuestionIndice)
                        file.delete()
                    }
                    questions
                }
                .subscribe()
    }

    interface chassePresenterView {
        fun listChasseDl(listChasse: List<Chasse>)
        fun listChasseError(throwable: Throwable)

        fun deleted()
    }

}