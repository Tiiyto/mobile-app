package com.example.thdf.presenter

import android.content.Context
import androidx.room.Room
import com.example.thdf.dao.CurrentChasseDao
import com.example.thdf.database.RoomDatabase
import com.example.thdf.model.CurrentChasse
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import java.io.File

class ToCachePresenter(private val view: ToCachePresenterView) {
    fun getCurrentChasse(context: Context) {
        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val currentDao: CurrentChasseDao = database.currentChasseDao()
        currentDao.get()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ currentChasse ->
                    view.CurrentChasse(currentChasse)
                }, { error ->
                    view.CurrentChasseError(error)
                })
    }

    fun deleteCurrent(context: Context)
    {
        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val currentDao: CurrentChasseDao = database.currentChasseDao()
        currentDao.get()
                .map { currentchasse ->
                    if(!currentchasse.fromDB)
                    {
                        currentchasse.questions?.forEach { question ->
                            val file = File(question.QuestionIndice)
                            file.delete()
                        }
                    }
                    currentchasse
                }
                .flatMapCompletable { currentChasse ->
                    currentDao.delete(currentChasse)
                }
                .subscribe()
    }
    interface ToCachePresenterView {
        fun CurrentChasse(currentChasse: CurrentChasse)
        fun CurrentChasseError(throwable: Throwable)
    }
}