package com.example.thdf.presenter

import android.content.Context
import android.util.Log
import androidx.room.Room
import com.example.thdf.CurrentChasseApplication
import com.example.thdf.DownloadAndSaveImageTask
import com.example.thdf.dao.CacheDao
import com.example.thdf.dao.ChasseDao
import com.example.thdf.dao.CurrentChasseDao
import com.example.thdf.database.RoomDatabase
import com.example.thdf.model.CurrentChasse
import com.example.thdf.model.Question
import com.example.thdf.service.RetrofitBuilder
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.annotations.NonNull
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.io.File


class DetailsPresenter (private val view: DetailsPresenterView){
    fun downloadAll(context: Context)
    {
        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val questionDao = database.questionDao()
        val chasseDao : ChasseDao = database.chasseDao()
        val cacheDao : CacheDao = database.cacheDao()
        val chasseService = RetrofitBuilder().getChasseService()
        chasseService.getTrack(CurrentChasseApplication.currentChasse!!.TrackID)
                .subscribeOn(Schedulers.io())
                .map { track ->
                    track.Questions.forEach { question ->
                        val url = DownloadAndSaveImageTask(context).execute(question.QuestionIndice,track.Chasse.TrackID.toString(),question.QuestionID.toString()).get()
                        question.QuestionIndice = url.toString()
                    }
                    track
                }
                .flatMapCompletable { track ->
                    cacheDao.insert(track.Cachette)
                            .andThen(chasseDao.insert(track.Chasse))
                            .andThen(questionDao.insert(track.Questions))
                }

                .subscribe({}, { error ->
                    view.downloadAllError(error)
                })
    }
    fun deleteAll(context: Context)
    {

        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val questionDao = database.questionDao()
        val chasseDao : ChasseDao = database.chasseDao()
        val cacheDao : CacheDao = database.cacheDao()
        /*
        * Suppression des questions
        * */
        questionDao.getAllQuestionFromTrack(CurrentChasseApplication.currentChasse!!.TrackID)
                .map { questions ->
                    questions.forEach { question ->
                        val file = File(question.QuestionIndice)
                        file.delete()
                    }
                    questions
                }
                .flatMapCompletable { questions ->
                    questionDao.delete(questions)
                }
                .subscribe()
        /*
        * Suppression de la cachette
        * */
        cacheDao.getCacheFromTrack(CurrentChasseApplication.currentChasse!!.TrackID)
                .flatMapCompletable { cache ->
                    cacheDao.delete(cache)
                }
                .subscribe()
        /* Suppression de la chasse
        * */
        chasseDao.getChasseById(CurrentChasseApplication.currentChasse!!.TrackID)
                .flatMapCompletable { chasse ->
                    chasseDao.delete(chasse)
                }
                .subscribe()
    }

    fun getAllQuestionFromTrack(context: Context) {
        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val questionDao = database.questionDao()
        questionDao.getAllQuestionFromTrack(CurrentChasseApplication.currentChasse!!.TrackID)
                .flatMap { listQuestion->
                    if (listQuestion.isNotEmpty()) {
                        insertWithQuestions(context,listQuestion)
                    } else {
                        insertCurrentChasse(context)
                    }
                }
                .subscribeOn(Schedulers.io())
                .subscribe({
                    view.insertCurrent()
                }, {error ->
                    view.insertCurrentError(error)

                })
    }

    fun insertCurrentChasse(context: Context):  Observable<Unit> {
        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val currentDao: CurrentChasseDao = database.currentChasseDao()
        val chasseService = RetrofitBuilder().getChasseService()
        return chasseService.getTrack(CurrentChasseApplication.currentChasse!!.TrackID)
                .map { track ->
                    track.Questions.forEach { question ->
                        val url = DownloadAndSaveImageTask(context).execute(question.QuestionIndice,track.Chasse.TrackID.toString(),question.QuestionID.toString()).get()
                        question.QuestionIndice = url.toString()
                    }
                    track
                }
                .flatMap { track ->
                    val current = CurrentChasse(
                            1,
                            CurrentChasseApplication.currentChasse,
                            track.Questions,
                            0,
                            track.Cachette,
                            3, false, false, false
                    )
                    currentDao.insert(current).andThen(Observable.just(Unit))
                }
    }

    fun insertWithQuestions(context: Context, questions: List<Question>) :  Observable<Unit> {
        val database = Room.databaseBuilder(context, RoomDatabase::class.java, "mydb").build()
        val cacheDao = database.cacheDao()
        val currentDao: CurrentChasseDao = database.currentChasseDao()
        return cacheDao.getCacheFromTrack(CurrentChasseApplication.currentChasse!!.TrackID)
                .flatMap{ cache ->
                    val current = CurrentChasse(
                            1,
                            CurrentChasseApplication.currentChasse,
                            questions  as ArrayList<Question>? ,
                            0,
                            cache,
                            3,true, false, false
                    )
                    currentDao.insert(current).andThen(Observable.just(Unit))
                }
    }

    interface DetailsPresenterView
    {
        fun insertCurrentError(throwable: Throwable)
        fun insertCurrent()

        fun getAllQuestionError(throwable: Throwable)

        fun downloadAllError(throwable: Throwable)
    }
}