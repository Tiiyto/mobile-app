package com.example.thdf

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import com.example.thdf.model.User
import com.example.thdf.model.UserSignUp
import com.example.thdf.presenter.SignUpPresenter
import com.example.thdf.service.RetrofitBuilder
import com.google.gson.JsonParser
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_sign_up.*
import retrofit2.HttpException
import kotlin.math.log

class SignUpActivity : AppCompatActivity(), SignUpPresenter.SignUpPresenterView {

    val presenter: SignUpPresenter = SignUpPresenter(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        btn_signup.isEnabled = false
        et_pseudo.doAfterTextChanged {
            etChanged(et_pseudo.text.toString(),et_pseudo)
        }
        et_firstname.doAfterTextChanged {
            etChanged(et_firstname.text.toString(),et_firstname)
        }
        et_lastname.doAfterTextChanged {
            etChanged(et_lastname.text.toString(),et_lastname)
        }
        et_email.doAfterTextChanged {
            etChanged(et_email.text.toString(),et_email)
        }
        et_password.doAfterTextChanged {
            etChanged(et_password.text.toString(),et_password)
        }
        et_password_2.doAfterTextChanged {
            etChanged(et_password_2.text.toString(),et_password_2)
        }
        btn_signup.setOnClickListener {
            val user = UserSignUp(et_pseudo.text.toString(), et_password.text.toString(),
                    et_email.text.toString(), et_firstname.text.toString(), et_lastname.text.toString())
            presenter.signup(user)
        }
        tv_signup.setOnClickListener {
            finish()
        }
    }
    fun etChanged(et_value: String, et: EditText)
    {
        if (et_value.isEmpty()) {
            et.hint = getString(R.string.champs_requi)
            et.setHintTextColor(resources.getColor(R.color.red))
            et.hint
            btn_signup.isEnabled = false
        }
        if(et == et_email)
        {
            if(!isEmailNameValid(et_value))
            {
                tv_problem.visibility = View.VISIBLE
                tv_problem.text = getString(R.string.need_password)
                btn_signup.isEnabled = false
            }
            else
            {
                tv_problem.visibility = View.INVISIBLE
            }
        }
        else if(et == et_password )
        {
            if(et_value.length < 7 ) {
                tv_problem.visibility = View.VISIBLE
                tv_problem.text = getString(R.string.pass_sup_7)
                btn_signup.isEnabled = false
            }
            else
            {
                tv_problem.visibility = View.INVISIBLE
            }
        }
        else if (et == et_password_2)
        {
            if (et_password.text.toString() != et_value)
            {
                tv_problem.visibility = View.VISIBLE
                tv_problem.text = getString(R.string.password_match)
                btn_signup.isEnabled = false
            }
            else
            {
                tv_problem.visibility = View.INVISIBLE
            }
        }
        dataChanged()
    }
    fun dataChanged()
    {
        if (isEmailNameValid(et_email.text.toString()) && isPasswordValid(et_password.text.toString())
                && !et_pseudo.text.isEmpty() && !et_firstname.text.isEmpty() && !et_lastname.text.isEmpty() && et_password.text.toString() == et_password_2.text.toString())
        {
            btn_signup.isEnabled = true
        }
    }
    private fun isEmailNameValid(username: String): Boolean {
        if (username.contains('@')) {
            return Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            return false
        }
    }
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 7
    }

    override fun signUpUser(user: User) {
        Toast.makeText(this,"Vous êtes inscrit !", Toast.LENGTH_SHORT).show()
    }

    override fun signUpError(throwable: Throwable) {
        if ( throwable is HttpException)
        {
            val errorJsonString = throwable.response()?.errorBody()?.string()
            val message = JsonParser().parse(errorJsonString)
                    .asJsonObject["message"]
                    .asString
            Toast.makeText(this, message,Toast.LENGTH_SHORT).show()
        }
        else
        {
            Toast.makeText(this, throwable.message, Toast.LENGTH_SHORT).show()
        }
    }
}