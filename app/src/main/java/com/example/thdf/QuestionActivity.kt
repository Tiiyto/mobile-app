package com.example.thdf

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.thdf.Maps.ToCacheActivity
import com.example.thdf.Maps.ToQuestionActivity
import com.example.thdf.model.CurrentChasse
import com.example.thdf.presenter.QuestionPresenter
import kotlinx.android.synthetic.main.activity_cache.*
import kotlinx.android.synthetic.main.activity_question.*
import java.text.Normalizer

class QuestionActivity : AppCompatActivity() , QuestionPresenter.QuestionPresenterView {

    val presenter: QuestionPresenter = QuestionPresenter(this)
    lateinit var currentChasse: CurrentChasse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_question)
    }
    fun stripAccents(string: String): String? {
        var string = string
        string = Normalizer.normalize(string, Normalizer.Form.NFD)
        string = string.replace("\\p{InCombiningDiacriticalMarks}+".toRegex(), "")
        return string
    }

    override fun onResume() {
        presenter.getCurrentChasse(this)
        super.onResume()
    }

    fun checkNbJoker(currentChasse: CurrentChasse, isJokerActivate: Boolean){
        if(currentChasse.joker==0 && isJokerActivate){
            joker.text = getString(R.string.revoirJoker)
        }else if(currentChasse.questions?.get(currentChasse.currentQuestion!!)?.QuestionIndice  == "" || currentChasse.joker==0){
            joker.text = getString(R.string.Joker_non_dispo)
            joker.isEnabled = false
        }else if(currentChasse.joker>0 && !isJokerActivate){
            "${getString(R.string.joker)} (${currentChasse.joker})".also { joker.text = it }
        }else{
            joker.text = getString(R.string.revoirJoker)
        }
        joker.setOnClickListener {
            if(currentChasse.joker>0 && !isJokerActivate){
                presenter.activeJoker(this)
                presenter.decrementeJoker(this)
            }
            val intent = Intent(this, JokerActivity::class.java)
            startActivity(intent)
        }
    }

    override fun CurrentChasse(currentChasse: CurrentChasse) {
        question.text = currentChasse.questions?.get(currentChasse.currentQuestion!!)!!.QuestionDescription
        tv_numQuestion.text = "Question " + (currentChasse.currentQuestion?.plus(1)) +" sur " +currentChasse.questions.size + " :"

        this.currentChasse=currentChasse
        presenter.getJokerActivate(this)

        repondre.setOnClickListener {
            repondre(currentChasse)
        }
        reponse.setOnEditorActionListener { _, actionId, _ ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_SEND -> {
                    repondre(currentChasse)
                    true
                }
                else -> false
            }
        }
    }
    private fun repondre(currentChasse: CurrentChasse)
    {
        val rightAnswer = currentChasse.questions?.get(currentChasse.currentQuestion!!)?.QuestionAnswer?.trim()
        val finalAnswer = reponse.text.toString().dropLastWhile { !it.isLetter() && !it.isDigit() }
        if (stripAccents(finalAnswer).equals(stripAccents(rightAnswer!!), ignoreCase = true)) {
            presenter.desactiveJoker(this)
            if (currentChasse.currentQuestion != (currentChasse.questions.size.minus(1))) nextQuestion(currentChasse)
            else lastQuestion(currentChasse)
        } else {
            badAnswer.visibility = View.VISIBLE
        }
    }

    override fun CurrentChasseError(throwable: Throwable) {
        Toast.makeText(this, throwable.message, Toast.LENGTH_LONG).show()
    }

    override fun getJokerActivated(state: Boolean) {
        checkNbJoker(this.currentChasse, state)
    }

    override fun getJokerActivatedError(throwable: Throwable) {
        Toast.makeText(this, throwable.message, Toast.LENGTH_SHORT).show()
    }

    private fun nextQuestion(currentChasse: CurrentChasse) {
        presenter.incrementCurrentQuestion(this,currentChasse)
        val intent = Intent(this, ToQuestionActivity::class.java)
        ToQuestionActivity.MyClass.activity?.finish()
        startActivity(intent)
        finish()
    }

    private fun lastQuestion(currentChasse: CurrentChasse) {
        presenter.incrementCurrentQuestion(this,currentChasse)
        val intent = Intent(this, ToCacheActivity::class.java)
        ToQuestionActivity.MyClass.activity?.finish()
        startActivity(intent)
        finish()
    }
}