package com.example.thdf

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.airbnb.lottie.LottieAnimationView
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sp : SharedPreferences = getSharedPreferences("connection", Context.MODE_PRIVATE)
        connexionIfFirstTime(sp)

        val bottomNavigation: BottomNavigationView = findViewById(R.id.nav_bar)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        if(savedInstanceState==null){
            bottomNavigation.selectedItemId = R.id.action_home
        }

    }

    private fun connexionIfFirstTime(sp: SharedPreferences){
        val firstTime = sp.getBoolean("firstLaunch", true)

        if(firstTime){
            val intent = Intent(this, ConnectionActivity::class.java)
            startActivity(intent)
            val editor: SharedPreferences.Editor  = sp.edit()
            editor.putBoolean("firstLaunch", false).apply()
        }
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.action_home -> {
                    val homeFragment = HomeFragment()
                    showFragment(homeFragment)

                return@OnNavigationItemSelectedListener true
            }
            R.id.action_plus -> {
                    val plusFragment = PlusFragment()
                    showFragment(plusFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.action_compte -> {
                val prefs: SharedPreferences = getSharedPreferences("connection", MODE_PRIVATE)
                val isConnected: Boolean = prefs.getBoolean("isConnected", false)
                if (!isConnected) {
                        val compteFragment = CompteFragment()
                        showFragment(compteFragment)

                } else {
                        showFragment(ConnectedFragment())
                }
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun showFragment(frg: Fragment){
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, frg).commit()
    }

    override fun onBackPressed() {
        val bottomNavigation: BottomNavigationView = findViewById(R.id.nav_bar)
        val seletedItemId: Int = bottomNavigation.selectedItemId
        if(R.id.action_home != seletedItemId)
        {
            bottomNavigation.selectedItemId = R.id.action_home
        }else
        {
            super.onBackPressed()
        }
    }
}