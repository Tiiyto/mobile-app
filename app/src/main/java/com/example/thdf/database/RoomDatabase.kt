package com.example.thdf.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.thdf.dao.*
import com.example.thdf.model.*

@Database(entities = [Question::class,Cache::class, Chasse::class, CurrentChasse::class] , version = 1)
@TypeConverters(Converters::class)
abstract class RoomDatabase : RoomDatabase() {
    abstract fun questionDao(): QuestionDao
    abstract fun cacheDao(): CacheDao
    abstract fun chasseDao(): ChasseDao
    abstract fun currentChasseDao(): CurrentChasseDao

}