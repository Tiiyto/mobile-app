package com.example.thdf.database

import androidx.room.TypeConverter
import com.example.thdf.model.Cache
import com.example.thdf.model.Chasse
import com.example.thdf.model.Question
import com.google.gson.Gson
import com.fasterxml.jackson.module.kotlin.*
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


class Converters {
    @TypeConverter
    fun fromChasse(chasse: Chasse): String {
        val gson = Gson()
        return gson.toJson(chasse)
    }

    @TypeConverter
    fun toChasse(element: String): Chasse {
        val mapper = jacksonObjectMapper()
        return mapper.readValue<Chasse>(element)
    }

    @TypeConverter
    fun fromQuestions(questions: ArrayList<Question>): String {
        val gson = Gson()
        return gson.toJson(questions)
    }

    @TypeConverter
    fun toQuestions(element: String): ArrayList<Question> {
        val mapper = jacksonObjectMapper()
        return mapper.readValue<ArrayList<Question>>(element)
    }

    @TypeConverter
    fun fromChche(cache: Cache): String {
        val gson = Gson()
        return gson.toJson(cache)
    }

    @TypeConverter
    fun toCache(element: String): Cache {
        val mapper = jacksonObjectMapper()
        return mapper.readValue<Cache>(element)
    }

    @TypeConverter
    fun fromJoker(jokers: List<String>): String {
        val gson = Gson()
        return gson.toJson(jokers)
    }

    @TypeConverter
    fun toJoker(element: String): List<String> {
        val mapper = jacksonObjectMapper()
        return mapper.readValue(element)
    }
}
