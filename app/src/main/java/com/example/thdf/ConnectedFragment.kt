package com.example.thdf

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.example.thdf.adapter.chasseDoneRvAdapter
import com.example.thdf.model.CompletedChasse
import com.example.thdf.presenter.ChasseDonePresenter

class ConnectedFragment : Fragment(), ChasseDonePresenter.ChasseDonePresenterView{

    lateinit var prefs: SharedPreferences
    val presenter: ChasseDonePresenter = ChasseDonePresenter(this)
    lateinit var recycler: RecyclerView
    val chasseDone: ArrayList<CompletedChasse> = ArrayList();

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView: View = inflater.inflate(R.layout.fragment_connected, container, false)
        this.recycler = rootView.findViewById(R.id.rv_chassedone)
        recycler.adapter = chasseDoneRvAdapter(chasseDone, context!!)
        recycler.layoutManager = LinearLayoutManager(context)

        prefs = context!!.getSharedPreferences("connection", Context.MODE_PRIVATE)
        val id_user: Int = prefs.getInt("ID",0)
        val log_user: String? = prefs.getString("login","")
        val profile_pic : String? = prefs.getString("profile_picture", "")

        val tv_login = rootView.findViewById<TextView>(R.id.tv_login)
        tv_login.text = log_user
        presenter.getChassesDone(id_user)
        Glide.with(context!!)
                .load(profile_pic)
                .override(300, 300)
                .transform(CircleCrop())
                .into(rootView.findViewById<ImageView>(R.id.img_profil))

        /*Bouton de deconnexion*/
        val deco = rootView.findViewById<TextView>(R.id.btn_deco)
        deco.setOnClickListener {

            val editor: SharedPreferences.Editor = prefs.edit()
            editor.putBoolean("isConnected", false)
                    .putString("login", "")
                    .putInt("ID", 0)
                    .putString("profile_picture", "")
                    .apply()
            activity?.supportFragmentManager?.beginTransaction()
                    ?.replace(R.id.fragment_container, CompteFragment())
                    ?.commit()
        }

        return rootView
    }

    override fun listChasseDone(listChasseDone: List<CompletedChasse>) {
        chasseDone.addAll(listChasseDone)
        recycler.adapter?.notifyDataSetChanged()
    }

    override fun listChasseDoneError(throwable: Throwable) {
        Log.e("Pas de connexion","Il n'y as pas de connexion pour charger les chasses faites")
    }

}