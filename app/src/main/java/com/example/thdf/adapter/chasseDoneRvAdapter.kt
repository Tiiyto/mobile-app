package com.example.thdf.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.thdf.R
import com.example.thdf.model.CompletedChasse

class chasseDoneRvAdapter(val chasseDone: List<CompletedChasse>, var context: Context) : RecyclerView.Adapter<chasseDoneRvAdapter.ViewHolder>()  {

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val levelIv: ImageView= itemView.findViewById(R.id.img_rv_chassedone)
        val chasseDoneTv: TextView = itemView.findViewById(R.id.tv_chassedone)
        val totalTv: TextView = itemView.findViewById(R.id.tv_total)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater : LayoutInflater = LayoutInflater.from(context)
        val chasseView : View = inflater.inflate(R.layout.item_cardview_chassedone, parent, false)
        return ViewHolder(chasseView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var img = 0
        when (chasseDone[position].Level) {
            "1" -> img = R.drawable.level1
            "2" -> img = R.drawable.level2
            "3" -> img = R.drawable.level3
            "4" -> img = R.drawable.level4
            "5" -> img = R.drawable.level5
        }
        Glide.with(context)
                .load(img)
                .into(holder.levelIv)
        holder.chasseDoneTv.text = chasseDone[position].Chasse_effectuee
        holder.totalTv.text = chasseDone[position].Total

    }

    override fun getItemCount(): Int {
        return chasseDone.size
    }
}