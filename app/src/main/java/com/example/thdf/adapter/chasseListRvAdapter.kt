package com.example.thdf.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.thdf.R
import com.example.thdf.model.Chasse

class chasseListRvAdapter(val chasses: List<Chasse>, var context: Context, private val listener: (Chasse) -> Unit) : RecyclerView.Adapter<chasseListRvAdapter.ViewHolder>()  {

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val titre: TextView = itemView.findViewById(R.id.tv_titre_chasse)
        val ville: TextView = itemView.findViewById(R.id.tv_ville_chasse)
        val bike: ImageView = itemView.findViewById(R.id.item_bike)
        val family: ImageView = itemView.findViewById(R.id.item_family)
        val level: ImageView= itemView.findViewById(R.id.img_level)
        val km: TextView = itemView.findViewById(R.id.tv_km)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater : LayoutInflater = LayoutInflater.from(context)
        val chasseView : View = inflater.inflate(R.layout.item_cardview_chasse, parent, false)
        return ViewHolder(chasseView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.titre.text = chasses[position].NameTrack
        holder.ville.text = chasses[position].City
        if(chasses[position].Track_Bike ==1) holder.bike.visibility = View.VISIBLE
        else holder.bike.visibility = View.GONE
        if(chasses[position].Track_Famille ==1) holder.family.visibility = View.VISIBLE
        else holder.family.visibility = View.GONE
        var img = 0
        when (chasses[position].Level) {
            1 -> img = R.drawable.level1
            2 -> img = R.drawable.level2
            3 -> img = R.drawable.level3
            4 -> img = R.drawable.level4
            5 -> img = R.drawable.level5
        }
        Glide.with(context)
                .load(img)
                .into(holder.level)
        holder.km.text = chasses[position].Distance.toString() + "km"
        holder.itemView.setOnClickListener{listener(chasses[position])}
    }

    override fun getItemCount(): Int {
        return chasses.size
    }
}

class chasseListRvAdapter2(val chasses: List<Chasse>, var context: Context) : RecyclerView.Adapter<chasseListRvAdapter2.ViewHolder>()  {

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val titre: TextView = itemView.findViewById(R.id.tv_titre_chasse)
        val ville: TextView = itemView.findViewById(R.id.tv_ville_chasse)
        val bike: ImageView = itemView.findViewById(R.id.item_bike)
        val family: ImageView = itemView.findViewById(R.id.item_family)
        val level: ImageView= itemView.findViewById(R.id.img_level)
        val km: TextView = itemView.findViewById(R.id.tv_km)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater : LayoutInflater = LayoutInflater.from(context)
        val chasseView : View = inflater.inflate(R.layout.item_cardview_chasse, parent, false)
        return ViewHolder(chasseView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.titre.text = chasses[position].NameTrack
        holder.ville.text = chasses[position].City
        if(chasses[position].Track_Bike ==1) holder.bike.visibility = View.VISIBLE
        else holder.bike.visibility = View.GONE
        if(chasses[position].Track_Famille ==1) holder.family.visibility = View.VISIBLE
        else holder.family.visibility = View.GONE
        var img = 0
        when (chasses[position].Level) {
            1 -> img = R.drawable.level1
            2 -> img = R.drawable.level2
            3 -> img = R.drawable.level3
            4 -> img = R.drawable.level4
            5 -> img = R.drawable.level5
        }
        Glide.with(context)
                .load(img)
                .into(holder.level)
        holder.km.text = chasses[position].Distance.toString() + "km"
    }

    override fun getItemCount(): Int {
        return chasses.size
    }
}