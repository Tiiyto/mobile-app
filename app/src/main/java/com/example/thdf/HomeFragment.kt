package com.example.thdf

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.thdf.Maps.MapsActivity

class HomeFragment() : Fragment() {

    private var currentToast: Toast? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val view: View = inflater.inflate(R.layout.fragment_home, container, false)
        val imgv_marcheur: ImageView = view.findViewById(R.id.marcheur)
        val btn_listchasse: Button = view.findViewById(R.id.list)
        /*Logo*/
        val logo: ImageView = view.findViewById(R.id.iv_logo)
        Glide.with(this).load(R.drawable.logo).into(logo)
        /* Background */
        Glide.with(this).load(R.drawable.homelogo).into(imgv_marcheur)

        /*
        * Bouton voir toutes les chasses MAP
        * */
        val goMap: Button = view.findViewById(R.id.button)
        goMap.setOnClickListener {
            val intent = Intent(context, MapsActivity::class.java)
            startActivity(intent)
        }
        /*
        * Bouton voir toutes les chasses cardView
        * */
        btn_listchasse.setOnClickListener {
            val intent = Intent(context,ListChasse::class.java)
            startActivity(intent)
        }

        /*imgv_marcheur.post {
            /*Test spotlight*/

            val firstRoot = FrameLayout(context!!)
            val first = layoutInflater.inflate(R.layout.layout_target, firstRoot)
            val firstTarget = Target.Builder()
                .setAnchor(view?.findViewById<View>(R.id.list)!!)
                .setShape(Circle(500f))
                .setOverlay(first)
                .setOnTargetListener(object : OnTargetListener {
                    override fun onStarted() {
                    }
                    override fun onEnded() {
                    }
                })
                .build()

            val spotlight = Spotlight.Builder(requireActivity())
                .setTargets(firstTarget)
                .setBackgroundColor(R.color.spotlightBackground)
                .setDuration(1000L)
                .setAnimation(DecelerateInterpolator(2f))
                .setOnSpotlightListener(object : OnSpotlightListener {
                    override fun onStarted() {
                    }
                    override fun onEnded() {
                    }
                })
                .build()

            spotlight.start()

            /*Sportlight fin*/
        }*/

        return view
    }



}