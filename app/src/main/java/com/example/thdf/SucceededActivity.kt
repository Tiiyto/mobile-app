package com.example.thdf

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.Glide
import com.example.thdf.Maps.ToCacheActivity
import com.example.thdf.Maps.ToChasse
import com.example.thdf.model.CurrentChasse
import com.example.thdf.model.PostChasseDone
import com.example.thdf.presenter.CurrentChassePresenter
import com.example.thdf.presenter.SucceededPresenter
import com.example.thdf.service.RetrofitBuilder
import com.google.gson.JsonParser
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_succeeded.*
import retrofit2.HttpException
import java.text.SimpleDateFormat
import java.util.*

class SucceededActivity : AppCompatActivity(), SucceededPresenter.SucceededPresenterView {

    val presenter: SucceededPresenter = SucceededPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_succeeded)

        Glide.with(this).load(R.drawable.trophee).into(img_trophee)

        presenter.getCurrentChasse(this)

    }

    override fun onBackPressed() {
        //ne rien faire
    }

    private fun insertInDatabase(currentChasse: CurrentChasse) {
        val date: String = SimpleDateFormat("yyyy-MM-dd").format(Date())
        var user_id  = 0
        val prefs: SharedPreferences = this.getSharedPreferences("connection", Context.MODE_PRIVATE)
        if(prefs.getBoolean("isConnected",false)) user_id = prefs.getInt("ID",0)
        val info = PostChasseDone(user_id, currentChasse.currentChasse?.NameTrack!!, currentChasse.currentChasse.Level , date)
        presenter.postChasseDone(info)
    }

    private fun end(currentChasse: CurrentChasse){
        //ajout dans la bdd wordpress de la fin de la chasse
        insertInDatabase(currentChasse)

        val intent = Intent(this, ToCacheActivity::class.java)
        startActivity(intent)
        this.finish()
    }

    override fun CurrentChasse(currentChasse: CurrentChasse) {
        btn_continue.setOnClickListener {
            end(currentChasse)
        }
    }

    override fun CurrentChasseError(throwable: Throwable) {
        Toast.makeText(this, throwable.message, Toast.LENGTH_SHORT).show()
    }

    override fun postChasseDoneError(throwable: Throwable) {
        if (throwable is HttpException) {
            val errorJsonString = throwable.response()?.errorBody()?.string()
            val message = JsonParser().parse(errorJsonString)
                    .asJsonObject["message"]
                    .asString
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, throwable.message, Toast.LENGTH_SHORT).show()
        }
    }
}