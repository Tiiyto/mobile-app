package com.example.thdf.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "chasse_table")
data class Chasse (
        @PrimaryKey val TrackID: Int,
        val NameTrack: String,
        val DescriptionShort: String,
        val Distance: Double,
        val Track_Time: String,
        val NbQuestion: Int,
        val City: String,
        val Adress: String,
        val Postal: Int,
        val LatTrack: Double,
        val LongTrack: Double,
        val Level: Int,
        val Level_Questions: Int,
        val Level_Physique: Int,
        val Track_Opened: Int,
        val Track_Bike: Int,
        val Track_Famille: Int,
        val Track_Code: String,
        val Track_Orientation: Int
)
