package com.example.thdf.model

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "current_chasse_table")
data class CurrentChasse(
        @PrimaryKey(autoGenerate = true)
        val id: Int,
        val currentChasse: Chasse?,
        val questions: ArrayList<Question>?,
        var currentQuestion: Int?,
        var cache: Cache?,
        var joker: Int = 3,
        val fromDB: Boolean,
        val cacheEnded: Boolean,
        val isJokerActivated: Boolean
)
