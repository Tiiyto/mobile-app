package com.example.thdf.model

data class User(
        val user_ID: Int?,
        val user_login: String,
        val user_pass: String,
        val user_nicename: String,
        val user_email: String,
        val user_display_name: String,
        val picture_URL: String
)
{
    constructor(user_login: String, user_pass: String) : this(null, user_login, user_pass,"","","","")
}