package com.example.thdf.model

data class CompletedChasse(
        val Level : String,
        val Chasse_effectuee : String,
        val Total: String,
)