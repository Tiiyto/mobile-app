package com.example.thdf.model

import androidx.room.Entity
import androidx.room.PrimaryKey

data class Track (
        val Chasse: Chasse,
        val Questions: ArrayList<Question>,
        val Cachette: Cache
)