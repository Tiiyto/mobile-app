package com.example.thdf.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cache_table")
data class Cache (
        @PrimaryKey val TrackID: Int,
        val CachLat: Double,
        val CachLong: Double,
        val CachDescription: String,
        val CachPartner: String,
        val CachImage: String,
        val CachCode: String
) {
    constructor(TrackID: Int) : this(TrackID,0.0,0.0,"","","","")
}