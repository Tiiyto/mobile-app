package com.example.thdf.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "question_table")
data class Question (
        val TrackID: Int,
        val QuestionID: Int,
        val QuestionDescription: String,
        val QuestionAnswer: String,
        val QuestionLat: Double,
        val QuestionLong: Double,
        val QuestionImage: String,
        var QuestionIndice: String,
        @PrimaryKey val TotalQuestionID: Int
)