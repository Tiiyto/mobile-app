package com.example.thdf.model

data class UserSignUp(
        val user_login: String,
        val user_pass: String,
        val user_email: String,
        val first_name: String,
        val last_name: String
)
