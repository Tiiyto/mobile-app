package com.example.thdf

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.thdf.adapter.chasseListRvAdapter
import com.example.thdf.adapter.chasseListRvAdapter2
import com.example.thdf.dialog.DetailsFragment
import com.example.thdf.model.Chasse
import com.example.thdf.presenter.ChasseApiPresenter
import com.takusemba.spotlight.OnSpotlightListener
import com.takusemba.spotlight.OnTargetListener
import com.takusemba.spotlight.Spotlight
import com.takusemba.spotlight.Target
import com.takusemba.spotlight.shape.RoundedRectangle
import kotlinx.android.synthetic.main.activity_list_chasse.*

class ListChasse : AppCompatActivity(), ChasseApiPresenter.ChasseApiPresenterView {

    lateinit var recycler: RecyclerView
    lateinit var adapter: chasseListRvAdapter
    val presenter: ChasseApiPresenter = ChasseApiPresenter(this)
    val chasses: ArrayList<Chasse> = ArrayList()
    val chassesGlobal: ArrayList<Chasse> = ArrayList()
    val chassesBike: ArrayList<Chasse> = ArrayList()
    val chassesFamille: ArrayList<Chasse> = ArrayList()
    val chassesFamillyAndBike: ArrayList<Chasse> = ArrayList()

    lateinit var progressBar: ProgressBar
    lateinit var spotlight: Spotlight
    private val targets: ArrayList<Target> = ArrayList()
    private var onTuto: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_chasse)

        this.recycler = findViewById(R.id.rv_chasse)
        this.adapter = chasseListRvAdapter(chasses, this) {
            CurrentChasseApplication.currentChasse = it
            val fm = supportFragmentManager
            val myFragment = DetailsFragment()
            myFragment.show(fm, "Simple Fragment")
        }

        progressBar = findViewById(R.id.pb_list_chasse)
        recycler.adapter = adapter
        recycler.layoutManager = LinearLayoutManager(this)
        presenter.getChassesApi()
        val switchbike: Switch = findViewById(R.id.switch_bike)
        val switchfamille: Switch = findViewById(R.id.switch_familly)
        switchbike.setOnCheckedChangeListener { buttonView, isChecked ->
            if (buttonView.isPressed) {
                if (isChecked) {
                    if (switchfamille.isChecked) show(chassesFamillyAndBike)
                    else show(chassesBike)
                } else {
                    if (switchfamille.isChecked) show(chassesFamille)
                    else show(chassesGlobal)
                }
            }
        }
        switchfamille.setOnCheckedChangeListener { buttonView, isChecked ->
            if (buttonView.isPressed) {
                if (isChecked) {
                    if (switchbike.isChecked) show(chassesFamillyAndBike)
                    else show(chassesFamille)
                } else {
                    if (switchbike.isChecked) show(chassesBike)
                    else show(chassesGlobal)
                }
            }
        }

        val sp : SharedPreferences = getSharedPreferences("firstUse", Context.MODE_PRIVATE)
        val firstTime = sp.getBoolean("firstList", true)

        if(firstTime){
            /*Afficher le tuto*/
            switchfamille.post{
                createSpotlight()
            }
            val editor: SharedPreferences.Editor  = sp.edit()
            editor.putBoolean("firstList", false).apply()
        }

    }

    override fun onBackPressed() {
        if(onTuto) run {
            spotlight.finish()
        }else{
            super.onBackPressed()
        }
    }

    private fun show(chasseReceive: ArrayList<Chasse>) {
        chasses.clear()
        chasses.addAll(chasseReceive)
        recycler.adapter?.notifyDataSetChanged()
    }

    override fun listChasseApi(listChasse: List<Chasse>) {
        chasses.addAll(listChasse)
        chassesGlobal.addAll(listChasse)
        listChasse.forEach { chasse ->
            if (chasse.Track_Bike == 1) chassesBike.add(chasse)
            if (chasse.Track_Famille == 1) chassesFamille.add(chasse)
            if (chasse.Track_Bike == 1 && chasse.Track_Famille == 1) chassesFamillyAndBike.add(
                chasse
            )
        }
        progressBar.visibility = View.GONE
        recycler.adapter?.notifyDataSetChanged()
    }

    override fun listChasseError(throwable: Throwable) {
        progressBar.visibility = View.GONE
        val btn_redo: Button = findViewById(R.id.btn_redo)
        val text_internet: TextView = findViewById(R.id.tv_no_internet)
        btn_redo.visibility = View.VISIBLE
        text_internet.visibility = View.VISIBLE

        btn_redo.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            btn_redo.visibility = View.GONE
            text_internet.visibility =  View.GONE
            presenter.getChassesApi()
        }
    }

    private fun createSpotlight(){

        val adapterNoClick = chasseListRvAdapter2(chasses, this)

        /*Création des targets*/
        //bike
        val firstRoot = FrameLayout(this)
        val first = layoutInflater.inflate(R.layout.layout_target, firstRoot)
        val target = Target.Builder()
            .setAnchor(this.findViewById<View>(R.id.switch_bike))
            .setShape(RoundedRectangle(180f,450f,100f))
            .setOverlay(first)
            .setOnTargetListener(object : OnTargetListener {
                override fun onStarted() {
                    switch_bike.isChecked = true
                    findViewById<TextView>(R.id.tuto_text).text = getString(R.string.text_tuto_bike)
                    switch_familly.isEnabled=false
                    recycler.adapter=adapterNoClick
                    onTuto = true
                }

                override fun onEnded() {
                    switch_bike.isChecked = false
                    switch_familly.isEnabled=true
                }
            })
            .build()

        //familly
        val secondRoot = FrameLayout(this)
        val second = layoutInflater.inflate(R.layout.layout_target, secondRoot)
        val target2 = Target.Builder()
            .setAnchor(this.findViewById<View>(R.id.switch_familly))
            .setShape(RoundedRectangle(180f,450f,100f))
            .setOverlay(second)
            .setOnTargetListener(object : OnTargetListener {
                override fun onStarted() {
                    switch_familly.isChecked=true
                    switch_bike.isEnabled=false
                    findViewById<TextView>(R.id.tuto_text).text = getString(R.string.text_tuto_familly)
                }
                override fun onEnded() {
                    switch_familly.isChecked=false
                    switch_bike.isEnabled=true
                }
            })
            .build()

        // Ajout des target
        targets.add(target)
        targets.add(target2)

        /*Création du spotlight*/
        spotlight = Spotlight.Builder(this)
            .setTargets(targets)
            .setBackgroundColor(R.color.spotlightBackground)
            .setDuration(2000L)
            .setAnimation(DecelerateInterpolator(2f))
            .setOnSpotlightListener(object : OnSpotlightListener {
                override fun onStarted() {
                }
                override fun onEnded() {
                    switch_bike.isChecked=false
                    switch_familly.isChecked=false
                    switch_bike.isEnabled=true
                    switch_familly.isEnabled=true
                    onTuto=false
                    recycler.adapter=adapter
                }
            })
            .build()

        spotlight.start()

        val nextTarget = View.OnClickListener { spotlight.next() }
        val lastTarget = View.OnClickListener { spotlight.previous() }
        val closeSpotlight = View.OnClickListener { spotlight.finish() }

        first.findViewById<View>(R.id.tuto_next).setOnClickListener(nextTarget)
        second.findViewById<View>(R.id.tuto_next).setOnClickListener(nextTarget)

        second.findViewById<View>(R.id.tuto_prev).setOnClickListener(lastTarget)

        first.findViewById<View>(R.id.stop_tuto).setOnClickListener(closeSpotlight)
        second.findViewById<View>(R.id.stop_tuto).setOnClickListener(closeSpotlight)

    }

}