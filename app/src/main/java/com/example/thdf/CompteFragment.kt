package com.example.thdf

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.thdf.model.User
import com.example.thdf.presenter.LoginPresenter
import com.google.gson.JsonParser
import kotlinx.android.synthetic.main.activity_connection.*
import retrofit2.HttpException
import java.util.*
import kotlin.concurrent.schedule

class CompteFragment : Fragment(), LoginPresenter.LoginPresenterView {
    val presenter: LoginPresenter = LoginPresenter(this)
    lateinit var progressBar: ProgressBar
    lateinit var rootView: View
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        rootView = inflater.inflate(R.layout.fragment_compte, container, false)
        val tv_signup: TextView = rootView.findViewById(R.id.tv_signup)
        progressBar = rootView.findViewById(R.id.progress_login)
        val etUsername = rootView.findViewById<EditText>(R.id.et_log)
        val etPass = rootView.findViewById<EditText>(R.id.et_password)
        val pass_oublie = rootView.findViewById<TextView>(R.id.pass_forgotten)
        val bouton_connnect: Button = rootView.findViewById(R.id.btn_login)
        etPass.setOnEditorActionListener { _, actionId, _ ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_SEND -> {
                    closeKeyBoard()
                    val user = User(etUsername.text.toString(), etPass.text.toString())
                    progressBar.visibility = View.VISIBLE
                    presenter.login(user)
                    true
                }
                else -> false
            }
        }
        tv_signup.setOnClickListener {
            val intent = Intent(context, SignUpActivity::class.java)
            startActivity(intent)
        }
        bouton_connnect.setOnClickListener {
            val user = User(etUsername.text.toString(), etPass.text.toString())
            progressBar.visibility = View.VISIBLE
            presenter.login(user)
        }
        pass_oublie.setOnClickListener {
            oubliePassword()
        }
        return rootView
    }
    private fun oubliePassword() {
        val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://tresorsdeshautsdefrance.com/mot-de-passe-oublie/")
        )
        startActivity(intent)
    }

    fun closeKeyBoard() {
        val view = rootView.findFocus()
        if (view != null) {
            val imm: InputMethodManager =
                    (activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    override fun loginUser(user: User) {
        progressBar.visibility = View.GONE
        val prefs: SharedPreferences = context?.getSharedPreferences("connection", AppCompatActivity.MODE_PRIVATE)!!
        val editor: SharedPreferences.Editor = prefs.edit()
        editor.putBoolean("isConnected", true)
        editor.putInt("ID", user.user_ID!!)
        editor.putString("login", user.user_login)
        editor.putString("profile_picture", user.picture_URL)
        editor.apply()
        changeFragment()
    }

    override fun loginError(throwable: Throwable) {
        progressBar.visibility = View.GONE
        val etPass = rootView.findViewById<EditText>(R.id.et_password)
        val etUsername = rootView.findViewById<EditText>(R.id.et_log)
        if (throwable is HttpException) {
            val errorJsonString = throwable.response()?.errorBody()?.string()
            val code = JsonParser().parse(errorJsonString)
                    .asJsonObject["code"]
                    .asString
            if (code == "empty_username" || code == "invalid_username") {
                etUsername.setBackgroundResource(R.drawable.wrong_input)
                Timer().schedule(700) {
                    etUsername.setBackgroundResource(R.drawable.rounded_input)
                }
            } else if (code == "incorrect_password" || code == "empty_password") {
                pass_forgotten.setTextColor(Color.WHITE)
                etPass.setBackgroundResource(R.drawable.wrong_input)
                Timer().schedule(700) {
                    etPass.setBackgroundResource(R.drawable.rounded_input)
                    pass_forgotten.setTextColor(Color.parseColor("#00670D"))
                }
            }
        } else {
            Toast.makeText(context, throwable.message, Toast.LENGTH_SHORT).show()
        }
    }
    fun changeFragment()
    {
        val prefs = context!!.getSharedPreferences("connection", Context.MODE_PRIVATE)
        if(prefs.getBoolean("isConnected", false)) {
            activity?.supportFragmentManager?.beginTransaction()
                    ?.replace(R.id.fragment_container, ConnectedFragment())
                    ?.commit()
        }
    }
    override fun onResume() {
        changeFragment()
        super.onResume()
    }
}