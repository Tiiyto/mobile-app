package com.example.thdf.service

import com.example.thdf.model.*
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path


interface ChasseService {
    companion object
    {
        val BASE_URL = "https://tresorsdeshautsdefrance.com/wp-json/restapi/v1/"
    }

    @GET("track/{id}")
    fun getTrack(@Path("id") id: Int?): Observable<Track>

    @GET("chasses/")
    fun listChasse (): Observable<List<Chasse>>

    @GET("chasses/{id}")
    fun getChassebyID(@Path("id") id: Int?): Observable<Chasse>

    @GET("questions/{trackID}")
    fun listQuestionByTrackId(@Path("trackID") trackID: Int?): Observable<List<Question>>

    @GET("caches/{trackID}")
    fun cacheByTrackID(@Path("trackID") trackID: Int?): Observable<Cache>

    @GET("/wp-json/restapi/v1/chasses/level/{niveau}")
    fun getListByLvl(@Path("niveau") niveau: Int?): Observable<List<Chasse>>?

    @GET("user/chassedone/{id}")
    fun getListChasseDone(@Path("id") id: Int?): Observable<List<CompletedChasse>>?

    @POST("user/chassedone")
    fun postChasseDone(@Body info: PostChasseDone?): Observable<Int>
}