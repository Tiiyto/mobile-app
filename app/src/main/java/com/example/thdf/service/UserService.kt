package com.example.thdf.service

import com.example.thdf.model.User
import com.example.thdf.model.UserSignUp
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.Body
import retrofit2.http.POST

interface UserService {
    companion object
    {
        val BASE_URL = "https://tresorsdeshautsdefrance.com/wp-json/restapi/v1/"
    }

    @POST("login")
    fun login(@Body login: User?): Observable<User>
    @POST("signup")
    fun signup(@Body login: UserSignUp?): Observable<User>

}