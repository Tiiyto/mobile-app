package com.example.thdf.service

import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

open class RetrofitBuilder {

    fun getChasseService(): ChasseService {
        return Retrofit.Builder()
            .baseUrl(ChasseService.BASE_URL)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ChasseService::class.java)
    }
    fun getUserService(): UserService {
        return Retrofit.Builder()
                .baseUrl(UserService.BASE_URL)
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(UserService::class.java)
    }
}