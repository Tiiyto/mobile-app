package com.example.thdf

import android.content.Context
import android.graphics.Bitmap
import android.os.AsyncTask
import com.bumptech.glide.Glide
import java.io.File
import java.io.FileOutputStream
import java.lang.ref.WeakReference

class DownloadAndSaveImageTask(context: Context) : AsyncTask<String, Unit, String>() {
    private var mContext: WeakReference<Context> = WeakReference(context)
    override fun doInBackground(vararg params: String?): String {
        val url = params[0]
        try {
            mContext.get()?.let {
                val bitmap = Glide.with(it)
                        .asBitmap()
                        .load(url)
                        .submit()
                        .get()
                try {
                    var file = it.getDir("Images", Context.MODE_PRIVATE)
                    val filename = "level_"+ params[1] + "_question_"+params[2]+".jpg"
                    file = File(file, filename)

                    val out = FileOutputStream(file)
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
                    out.flush()
                    out.close()
                    return  file.absolutePath.toString()
                } catch (e: Exception) {
                    return ""
                }
            }
        }catch (e: Exception){
            return ""
        }
        return ""
    }
}