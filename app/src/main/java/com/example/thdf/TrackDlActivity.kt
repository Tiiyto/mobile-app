package com.example.thdf

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.thdf.adapter.TrackDlAdapter
import com.example.thdf.adapter.chasseListRvAdapter
import com.example.thdf.dialog.DetailsFragment
import com.example.thdf.model.Chasse
import com.example.thdf.presenter.ChassePresenter
import kotlinx.android.synthetic.main.activity_track_dl.*
import kotlinx.android.synthetic.main.items_track_dl.*

class TrackDlActivity : AppCompatActivity(), ChassePresenter.chassePresenterView{

    lateinit var recycler: RecyclerView
    lateinit var adapter: TrackDlAdapter
    val presenter: ChassePresenter = ChassePresenter(this)
    val chasses : ArrayList<Chasse> = ArrayList()
    var onDeleting = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track_dl)

        this.recycler = findViewById(R.id.rv_track_dl)
        adapter = TrackDlAdapter(chasses, this)
        recycler.adapter = adapter
        btn_delete.setOnClickListener {
            if (chasses.size > 0 ){
                if(onDeleting) {
                    presenter.deleteListChasse(this,adapter.chasseSelected)
                }
                else {
                    btn_cancel.visibility = View.VISIBLE
                    adapter.deletable = true
                    onDeleting = true
                    adapter.notifyDataSetChanged()
                }
            }
        }
        btn_cancel.setOnClickListener {
            btn_cancel.visibility = View.INVISIBLE
            if(onDeleting) {
                adapter.deletable = false
                onDeleting = false
                adapter.notifyDataSetChanged()
            }
        }
        recycler.layoutManager = LinearLayoutManager(this)
        presenter.getChasses(this)
    }

    override fun listChasseDl(listChasse: List<Chasse>) {
        chasses.clear()
        chasses.addAll(listChasse)
        progressBar_dl.visibility = View.GONE
        adapter.notifyDataSetChanged()
        if(chasses.isEmpty()) tv_no_track_dl.text=getString(R.string.no_dl_track)
    }

    override fun listChasseError(throwable: Throwable) {
        Toast.makeText(this, throwable.message, Toast.LENGTH_SHORT).show()
    }

    override fun deleted() {
        adapter.deletable = false
        onDeleting = false
        presenter.getChasses(this)
        btn_cancel.visibility = View.INVISIBLE
    }
}