package com.example.thdf

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.room.Room
import com.example.thdf.Maps.ToCacheActivity
import com.example.thdf.Maps.ToChasse
import com.example.thdf.dao.CurrentChasseDao
import com.example.thdf.database.RoomDatabase
import com.example.thdf.model.CurrentChasse
import com.example.thdf.model.PostChasseDone
import com.example.thdf.presenter.CachePresenter
import com.example.thdf.presenter.CurrentChassePresenter
import com.example.thdf.service.RetrofitBuilder
import com.google.gson.JsonParser
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_cache.*
import kotlinx.android.synthetic.main.activity_question.*
import retrofit2.HttpException
import java.text.SimpleDateFormat
import java.util.*

class CacheActivity : AppCompatActivity() , CachePresenter.CachePresenterView {

    val presenter: CachePresenter = CachePresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cache)

        btn_check_cache.isEnabled = false
        et_chache.doAfterTextChanged {
            btn_check_cache.isEnabled = et_chache.text.isNotEmpty()
        }
        presenter.getCurrentChasse(this)
    }
    override fun CurrentChasse(currentChasse: CurrentChasse) {

        val tv_desc = findViewById<TextView>(R.id.tv_desc_cache)
        tv_desc.text = currentChasse.cache?.CachDescription

        btn_check_cache.setOnClickListener {
            checkAnswer(currentChasse)
            checkConnected()
        }
        /*clic sur valider clavier*/
        et_chache.setOnEditorActionListener { _, actionId, _ ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_SEND -> {
                    checkAnswer(currentChasse)
                    checkConnected()
                    true
                }
                else -> false
            }
        }
    }

    override fun CurrentChasseError(throwable: Throwable) {
        Toast.makeText(this, throwable.message, Toast.LENGTH_SHORT).show()
    }

    private fun checkAnswer(currentChasse: CurrentChasse) {
        val code = currentChasse.cache?.CachCode?.trim()
        val answer = et_chache.text.toString().dropLastWhile { !it.isLetter() && !it.isDigit() }
        if(answer.equals(code, ignoreCase = true)) {
            val intent = Intent(this, SucceededActivity::class.java)
            presenter.setTrackEnded(this)
            ToCacheActivity.MyClass.activity?.finish()
            startActivity(intent)
            finish()
        }
        else tv_cache_wrong_answer.visibility = View.VISIBLE
    }

    private fun checkConnected(){
        val prefs: SharedPreferences = getSharedPreferences("connection", Context.MODE_PRIVATE)
        if(!prefs.getBoolean("isConnected",false)){
            val intent = Intent(this, ConnectionActivity::class.java)
            startActivity(intent)
        }
    }
}
