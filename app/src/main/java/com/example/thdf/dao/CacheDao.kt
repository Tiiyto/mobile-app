package com.example.thdf.dao

import androidx.room.*
import com.example.thdf.model.Cache
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable

@Dao
interface CacheDao {
    @Query("SELECT * FROM cache_table WHERE TrackID = :trackId")
    fun getCacheFromTrack(trackId: Int) : Observable<Cache>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(cache : Cache) : Completable

    @Delete
    fun delete(cache: Cache) : Completable

    @Query("DELETE FROM cache_table WHERE TrackID IN (:ids)")
    fun deleteItemByIds(ids: List<Int>) : Completable
}