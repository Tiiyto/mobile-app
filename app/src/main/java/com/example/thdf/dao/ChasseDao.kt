package com.example.thdf.dao

import androidx.room.*
import com.example.thdf.model.Chasse
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable

@Dao

interface ChasseDao {

    @Query("SELECT * FROM chasse_table WHERE TrackID = :chasseID")
    fun getChasseById(chasseID : Int) : Observable<Chasse>

    @Query("SELECT * FROM chasse_table")
    fun getAllTracks() : Observable<List<Chasse>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chasse : Chasse): Completable

    @Delete
    fun delete(chasse: Chasse) : Completable

    @Query("DELETE FROM chasse_table WHERE TrackID IN (:ids)")
    fun deleteItemByIds(ids: List<Int>) : Completable
}