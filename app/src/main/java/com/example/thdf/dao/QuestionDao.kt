package com.example.thdf.dao

import androidx.room.*
import com.example.thdf.model.Question
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable

@Dao
interface QuestionDao {
    @Query("SELECT * FROM question_table WHERE TrackID= :id")
    fun getAllQuestionFromTrack(id: Int) : Observable<List<Question>>

    @Query("SELECT * FROM question_table WHERE QuestionID = :questionId")
    fun getQuestionById(questionId : Int) : Observable<Question>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(questions : List<Question>): Completable

    @Delete
    fun delete(questions: List<Question>) : Completable

    @Query("DELETE FROM question_table WHERE TrackID IN (:ids)")
    fun deleteItemByIds(ids: List<Int>) : Completable

    @Query("SELECT * FROM question_table WHERE TrackID IN (:ids)")
    fun getQuestionByIds(ids: List<Int>) : Observable<List<Question>>
}