package com.example.thdf.dao

import androidx.room.*
import com.example.thdf.model.Cache
import com.example.thdf.model.CurrentChasse
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

@Dao
interface CurrentChasseDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(currentChasse: CurrentChasse) : Completable

    @Delete
    fun delete(currentChasse: CurrentChasse) : Completable

    @Query("SELECT * FROM current_chasse_table WHERE id = 1")
    fun get() : Observable<CurrentChasse>

    @Query("UPDATE current_chasse_table SET currentQuestion = currentQuestion + 1 WHERE id = 1")
    fun incrementCurrentQuestion(): Completable

    @Query("UPDATE current_chasse_table SET joker = joker - 1 WHERE id = 1")
    fun decrementeJoker(): Completable

    @Query("UPDATE current_chasse_table SET cache = :cache WHERE id = 1")
    fun setCache(cache: Cache): Completable

    @Query("UPDATE current_chasse_table SET cacheEnded = 1 WHERE id = 1")
    fun setCacheEnded(): Completable

    @Query("UPDATE current_chasse_table SET isJokerActivated = 1 WHERE id = 1")
    fun activeJoker(): Completable

    @Query("UPDATE current_chasse_table SET isJokerActivated = 0 WHERE id = 1")
    fun desactiveJoker(): Completable

    @Query("SELECT isJokerActivated FROM current_chasse_table WHERE id=1")
    fun getJokerActivated(): Observable<Boolean>
}
