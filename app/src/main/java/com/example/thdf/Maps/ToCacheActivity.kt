package com.example.thdf.Maps

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.thdf.CacheActivity
import com.example.thdf.R
import com.example.thdf.model.CurrentChasse
import com.example.thdf.presenter.CurrentChassePresenter
import com.example.thdf.presenter.ToCachePresenter
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_to_cache.*

class ToCacheActivity : AppCompatActivity(), OnMapReadyCallback, ToCachePresenter.ToCachePresenterView {
    val presenter: ToCachePresenter = ToCachePresenter(this)
    class MyClass{
        companion object{
            var activity: Activity? = null
        }
    }
    companion object {
        private const val REQUEST_LOCATION_PERMISSION = 1
    }
    private lateinit var map: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_to_cache)
        MyClass.activity = this
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        presenter.getCurrentChasse(this)
        enableMyLocation()
    }
    override fun CurrentChasse(currentChasse: CurrentChasse) {
        val cache = currentChasse.cache
        if (cache != null ) {
            if( !currentChasse.cacheEnded) {
                val depart = LatLng(currentChasse.currentChasse?.LatTrack!!, currentChasse.currentChasse.LongTrack)
                map.addMarker(
                        MarkerOptions()
                                .title("Point de départ")
                                .position(depart)
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))
                )
                val cachepos = LatLng(cache.CachLat, cache.CachLong)
                map.addMarker(
                        MarkerOptions()
                                .title(getString(R.string.Cachette))
                                .position(cachepos)
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))

                ).showInfoWindow()
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(cachepos, 17.0f))
                imHere.setOnClickListener {
                    val intent = Intent(this, CacheActivity::class.java)
                    startActivity(intent)
                }
            }
            else
            {
                val depart = LatLng(currentChasse.currentChasse?.LatTrack!!, currentChasse.currentChasse.LongTrack)
                map.addMarker(
                        MarkerOptions()
                                .title("Point de départ")
                                .position(depart)
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))
                ).showInfoWindow()
                val cachepos = LatLng(cache.CachLat, cache.CachLong)
                map.addMarker(
                        MarkerOptions()
                                .title(getString(R.string.Cachette))
                                .position(cachepos)
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))

                )
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(depart, 17.0f))
                imHere.text = getString(R.string.end)
                imHere.setOnClickListener {
                    presenter.deleteCurrent(this)
                    this.finish()
                }
            }
        }
        currentChasse.questions?.first()
    }

    override fun CurrentChasseError(throwable: Throwable) {
        Toast.makeText(this, throwable.message, Toast.LENGTH_SHORT).show()
    }
    private fun isPermissionGranted() : Boolean {
        return ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }
    private fun enableMyLocation() {
        if (isPermissionGranted()) {
            map.isMyLocationEnabled = true
        }
        else {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION),
                    ToCacheActivity.REQUEST_LOCATION_PERMISSION
            )
        }
    }
    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray) {
        if (requestCode == ToCacheActivity.REQUEST_LOCATION_PERMISSION) {
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
                enableMyLocation()
            }
        }
    }
}