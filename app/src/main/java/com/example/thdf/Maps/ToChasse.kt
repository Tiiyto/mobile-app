package com.example.thdf.Maps

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.room.Room
import com.example.thdf.CurrentChasseApplication
import com.example.thdf.MainActivity
import com.example.thdf.R
import com.example.thdf.dao.CurrentChasseDao
import com.example.thdf.database.RoomDatabase
import com.example.thdf.model.CurrentChasse
import com.example.thdf.presenter.CurrentChassePresenter
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.takusemba.spotlight.OnSpotlightListener
import com.takusemba.spotlight.OnTargetListener
import com.takusemba.spotlight.Spotlight
import com.takusemba.spotlight.Target
import com.takusemba.spotlight.shape.Circle
import kotlinx.android.synthetic.main.activity_to_chasse.*

class ToChasse : AppCompatActivity(), OnMapReadyCallback, CurrentChassePresenter.CurrentChassePresenterView  {

    companion object {
        private const val REQUEST_LOCATION_PERMISSION = 1
    }

    val presenter: CurrentChassePresenter = CurrentChassePresenter(this)
    private lateinit var map: GoogleMap

    lateinit var spotlight: Spotlight
    private val targets: ArrayList<Target> = ArrayList()
    private var onTuto: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_to_chasse)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

    }

    override fun onBackPressed() {
        if(onTuto) run {
            spotlight.finish()
        }else{
            super.onBackPressed()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        presenter.getCurrentChasse(this)
        val database = Room.databaseBuilder(this, RoomDatabase::class.java, "mydb").build()
        val currentDao: CurrentChasseDao = database.currentChasseDao()
        val imReady: Button = findViewById(R.id.imReady)
        imReady.setOnClickListener {
            val intent = Intent(this, ToQuestionActivity::class.java)
            startActivity(intent)
            finish()
        }
        enableMyLocation()


        val sp : SharedPreferences = getSharedPreferences("firstUse", Context.MODE_PRIVATE)
        val firstTime = sp.getBoolean("firstToChasse", true)

        if(firstTime){
            /*Afficher le tuto*/
            center.post{
                createSpotlight()
            }
            val editor: SharedPreferences.Editor  = sp.edit()
            editor.putBoolean("firstToChasse", false).apply()
        }



    }
    private fun isPermissionGranted() : Boolean {
        return ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }
    private fun enableMyLocation() {
        if (isPermissionGranted()) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            map.isMyLocationEnabled = true
        }
        else {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_LOCATION_PERMISSION
            )
        }
    }
    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray) {
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
                enableMyLocation()
            }
        }
    }

    override fun CurrentChasse(currentChasse: CurrentChasse) {
        val beginChasse = LatLng(currentChasse.currentChasse?.LatTrack!!,currentChasse.currentChasse.LongTrack)
        map.addMarker(
                MarkerOptions()
                        .position(beginChasse)
                        .title(CurrentChasseApplication.currentChasse!!.NameTrack)
                        .snippet(CurrentChasseApplication.currentChasse!!.Adress + ", " +  CurrentChasseApplication.currentChasse!!.Postal + " " + CurrentChasseApplication.currentChasse!!.City)
        ).showInfoWindow()
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(beginChasse,13.0f))
    }
    override fun CurrentChasseError(throwable: Throwable) {
        Toast.makeText(this,throwable.message,Toast.LENGTH_SHORT).show()
    }

    private fun createSpotlight(){

        /*Création des targets*/
        //cercle sur la map
        val firstRoot = FrameLayout(this)
        val first = layoutInflater.inflate(R.layout.layout_target_map, firstRoot)
        val target = Target.Builder()
            .setAnchor(findViewById<View>(R.id.center))
            .setShape(Circle(250f))
            .setOverlay(first)
            .setOnTargetListener(object : OnTargetListener {
                override fun onStarted() {
                    val texte = findViewById<TextView>(R.id.tuto_text)
                    onTuto=true
                    texte.setTranslationY(-200f)
                    texte.text = getString(R.string.text_tuto_depart)
                    map.uiSettings.setAllGesturesEnabled(false)
                    imReady.isEnabled=false
                }
                override fun onEnded() {

                }
            })
            .build()

        // Ajout des target
        targets.add(target)

        /*Création du spotlight*/
        spotlight = Spotlight.Builder(this)
            .setTargets(targets)
            .setBackgroundColor(R.color.spotlightBackground)
            .setDuration(2000L)
            .setAnimation(DecelerateInterpolator(2f))
            .setOnSpotlightListener(object : OnSpotlightListener {
                override fun onStarted() {
                }
                override fun onEnded() {
                    onTuto=false
                    map.uiSettings.setAllGesturesEnabled(true)
                    imReady.isEnabled=true
                }
            })
            .build()

        spotlight.start()

        val closeSpotlight = View.OnClickListener { spotlight.finish() }

        first.findViewById<View>(R.id.stop_tuto).setOnClickListener(closeSpotlight)

    }

}