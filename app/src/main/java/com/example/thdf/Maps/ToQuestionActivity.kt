package com.example.thdf.Maps

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.thdf.QuestionActivity
import com.example.thdf.R
import com.example.thdf.model.CurrentChasse
import com.example.thdf.model.Question
import com.example.thdf.presenter.CurrentChassePresenter
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class ToQuestionActivity : AppCompatActivity(), OnMapReadyCallback, LocationListener , CurrentChassePresenter.CurrentChassePresenterView {
    val presenter: CurrentChassePresenter = CurrentChassePresenter(this)
    companion object {
        private const val REQUEST_LOCATION_PERMISSION = 1
    }
    class MyClass{
        companion object{
            var activity: Activity? = null
        }
    }
    private lateinit var map: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_to_question)
        MyClass.activity = this
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.uiSettings.isMapToolbarEnabled = false;
        presenter.getCurrentChasse(this)
        enableMyLocation()
    }
    private fun isPermissionGranted() : Boolean {
        return ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }
    private fun enableMyLocation() {
        if (isPermissionGranted()) {
            if (ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            map.isMyLocationEnabled = true
        }
        else {
            ActivityCompat.requestPermissions(this, arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_LOCATION_PERMISSION)
        }
    }
    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray) {
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
                enableMyLocation()
            }
        }
    }
    override fun onLocationChanged(location: Location) {
        val me = LatLng(location.latitude, location.longitude)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(me, 12.0f))
    }

    override fun CurrentChasse(currentChasse: CurrentChasse) {
        val currentQuestion = currentChasse.questions!![currentChasse.currentQuestion!!]
        val nextQuestion = LatLng(
                currentQuestion.QuestionLat,
                currentQuestion.QuestionLong
        )
        var tempQuestion: Question? = currentChasse.questions.first()

        val depart = LatLng(currentChasse.currentChasse?.LatTrack!!, currentChasse.currentChasse.LongTrack)
        map.addMarker(
                MarkerOptions()
                        .title("Point de départ")
                        .position(depart)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))
        )

        while (tempQuestion != currentQuestion) {
            val questionFaite = LatLng(tempQuestion!!.QuestionLat, tempQuestion.QuestionLong)
            map.addMarker(
                    MarkerOptions()
                            .title("Étape " + (currentChasse.questions.indexOf(tempQuestion) + 1))
                            .position(questionFaite)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
            )
            tempQuestion = currentChasse.questions[currentChasse.questions.indexOf(tempQuestion) + 1]
        }
        map.addMarker(
                MarkerOptions()
                        .position(nextQuestion)
                        .title("Destination de l'étape " + (currentChasse.questions.indexOf(currentQuestion) + 1))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
        ).showInfoWindow()

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(nextQuestion, 17.0f))

        val imHere: Button = findViewById(R.id.imHere)

        imHere.setOnClickListener {
            val intent = Intent(this, QuestionActivity::class.java)
            startActivity(intent)
        }
    }
    override fun CurrentChasseError(throwable: Throwable) {
        Toast.makeText(this, throwable.message, Toast.LENGTH_SHORT).show()
    }
}