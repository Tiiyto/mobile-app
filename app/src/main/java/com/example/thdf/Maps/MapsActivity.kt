package com.example.thdf.Maps

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.example.thdf.CurrentChasseApplication
import com.example.thdf.R
import com.example.thdf.dialog.DetailsFragment
import com.example.thdf.model.Chasse
import com.example.thdf.model.CurrentChasse
import com.example.thdf.presenter.MapsPresenter
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.example.thdf.service.ChasseService
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.takusemba.spotlight.OnSpotlightListener
import com.takusemba.spotlight.OnTargetListener
import com.takusemba.spotlight.Spotlight
import com.takusemba.spotlight.Target
import com.takusemba.spotlight.shape.Circle


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, LocationListener, MapsPresenter.MapsPresenterPresenterView  {
    val presenter: MapsPresenter = MapsPresenter(this)
    companion object {
        private const val REQUEST_LOCATION_PERMISSION = 1
    }
    private lateinit var map: GoogleMap
    lateinit var listChasseGlobal: List<Chasse>
    var mMarkerMap: HashMap<String, Int> = HashMap()

    var dialogIsActive = false
    var LocationNeededIsActive = false

    private val size = 110
    lateinit var progressBar : ProgressBar
    lateinit var spotlight: Spotlight
    private val targets: ArrayList<Target> = ArrayList()
    private var onTuto: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        progressBar = findViewById(R.id.progressBarMap)
        presenter.getListChasse()
        enableMyLocation()
        map.setOnMarkerClickListener { marker ->
            if(onTuto) run {
                spotlight.finish()
            }
            CurrentChasseApplication.currentChasse = listChasseGlobal.find{it.TrackID == mMarkerMap[marker.id]}
            val move = LatLng(CurrentChasseApplication.currentChasse!!.LatTrack, CurrentChasseApplication.currentChasse!!.LongTrack)
            map.moveCamera(CameraUpdateFactory.newLatLng(move))
            val fm = supportFragmentManager
            val myFragment = DetailsFragment()
            myFragment.show(fm, "Simple Fragment")
            true
        }


        val zoom = LatLng(50.590886, 2.093096)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(zoom, 9.0f))

        val sp : SharedPreferences = getSharedPreferences("firstUse", Context.MODE_PRIVATE)
        val firstTime = sp.getBoolean("firstMap", true)

        if(firstTime){
            /*Afficher le tuto*/
                createSpotlight()
            val editor: SharedPreferences.Editor  = sp.edit()
            editor.putBoolean("firstMap", false).apply()
        }

    }

    override fun onBackPressed() {
        if(onTuto) run {
            spotlight.finish()
        }else{
            super.onBackPressed()
        }

    }


    private fun isPermissionGranted() : Boolean {
        return ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }
    private fun enableMyLocation() {
        if (isPermissionGranted()) {
            if (ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            map.isMyLocationEnabled = true
        }
        else {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_LOCATION_PERMISSION
            )
        }
    }
    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray) {
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
                enableMyLocation()
            }
        }
    }

    override fun onLocationChanged(location: Location) {
        val me = LatLng(location.latitude, location.longitude)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(me, 12.0f))
    }

    override fun onResume() {
        super.onResume()
        LocationNeeded()
        if(!dialogIsActive) presenter.getCurrentChasse(this)

    }
    private fun LocationNeeded()
    {
        if(!LocationNeededIsActive)
        {
            val locManager: LocationManager
            locManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (!locManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                val builder: AlertDialog.Builder = AlertDialog.Builder(this)
                builder.setMessage("Le GPS n'est pas actif, voulez-vous l'activer ? ")
                        .setCancelable(false)
                        .setPositiveButton("Activer le GPS") { dialog, id ->
                            startActivity(Intent("android.settings.LOCATION_SOURCE_SETTINGS"))
                            LocationNeededIsActive = false
                        }
                        .setNegativeButton("Non merci") { dialog, id ->
                            dialog.dismiss()
                            LocationNeededIsActive = false
                        }
                val alert: AlertDialog = builder.create()
                alert.show()
                LocationNeededIsActive = true
            }
        }

    }

    private fun createSpotlight(){

        /*Création des targets*/
        //cercle sur la map
        val firstRoot = FrameLayout(this)
        val first = layoutInflater.inflate(R.layout.layout_target_map, firstRoot)
        val target = Target.Builder()
            .setAnchor((findViewById<View>(R.id.progressBarMap).x),(findViewById<View>(R.id.progressBarMap).y)-30)
            .setShape(Circle(400f))
            .setOverlay(first)
            .setOnTargetListener(object : OnTargetListener {
                override fun onStarted() {
                    onTuto=true
                    findViewById<TextView>(R.id.tuto_text).text = getString(R.string.text_tuto_map)
                    Glide.with(applicationContext).load(R.drawable.arrow).into(findViewById(R.id.image_arrow))
                    map.uiSettings.setAllGesturesEnabled(false)
                }
                override fun onEnded() {

                }
            })
            .build()

        // Ajout des target
        targets.add(target)

        /*Création du spotlight*/
        spotlight = Spotlight.Builder(this)
            .setTargets(targets)
            .setBackgroundColor(R.color.spotlightBackground)
            .setDuration(2000L)
            .setAnimation(DecelerateInterpolator(2f))
            .setOnSpotlightListener(object : OnSpotlightListener {
                override fun onStarted() {
                }
                override fun onEnded() {
                    onTuto=false
                    map.uiSettings.setAllGesturesEnabled(true)
                }
            })
            .build()

        spotlight.start()

        val closeSpotlight = View.OnClickListener { spotlight.finish() }

        first.findViewById<View>(R.id.stop_tuto).setOnClickListener(closeSpotlight)

    }

    override fun CurrentChasse(currentChasse: CurrentChasse) {
        if (currentChasse.currentChasse != null) {
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.chasse_en_cours))
                    .setMessage(getString(R.string.quesition_continuer))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.tocontinue)) { dialog, id ->
                        val intent: Intent = if (currentChasse.currentQuestion == currentChasse.questions?.size) {
                            Intent(this, ToCacheActivity::class.java)
                        } else {
                            Intent(this, ToQuestionActivity::class.java)
                        }
                        dialogIsActive = false
                        startActivity(intent)
                        dialog.dismiss()
                    }
                    .setNegativeButton(getString(R.string.abandonner)) { dialog, id ->
                        presenter.deleteCurrentChasse(this, currentChasse)
                        dialog.dismiss()
                        dialogIsActive = false
                    }
            val alert: AlertDialog = builder.create()
            alert.show()
            dialogIsActive = true
        }
    }

    override fun CurrentChasseError(throwable: Throwable) {
        Toast.makeText(this, throwable.message, Toast.LENGTH_SHORT).show()
    }

    override fun listChasse(listChasse: List<Chasse>) {
        listChasseGlobal = listChasse
        for (chasse in listChasse) {
            val temp = LatLng(chasse.LatTrack, chasse.LongTrack)
            val marker = map.addMarker(MarkerOptions().position(temp).title(chasse.NameTrack))
            lateinit var bitmap: Bitmap
            when (chasse.Level) {
                1 -> bitmap = BitmapFactory.decodeResource(this.resources, R.drawable.level1)
                2 -> bitmap = BitmapFactory.decodeResource(this.resources, R.drawable.level2)
                3 -> bitmap = BitmapFactory.decodeResource(this.resources, R.drawable.level3)
                4 -> bitmap = BitmapFactory.decodeResource(this.resources, R.drawable.level4)
                5 -> bitmap = BitmapFactory.decodeResource(this.resources, R.drawable.level5)

            }
            val bit = Bitmap.createScaledBitmap(bitmap, size, size, false)
            val bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bit)
            marker.setIcon(bitmapDescriptor)

            mMarkerMap[marker.id] = chasse.TrackID

        }
        progressBar.visibility = View.GONE
    }

    override fun listChasseError(throwable: Throwable) {
        presenter.getListChasseDb(this)
    }

    override fun listChasseDb(listChasse: List<Chasse>) {
        listChasseGlobal = listChasse
        if (listChasse.isNotEmpty()) {
            for (track in listChasseGlobal) {
                val temp = LatLng(track.LatTrack, track.LongTrack)
                val marker = map.addMarker(MarkerOptions().position(temp).title(track.NameTrack))
                lateinit var bitmap: Bitmap
                when (track.Level) {
                    1 -> bitmap = BitmapFactory.decodeResource(this.resources, R.drawable.level1)
                    2 -> bitmap = BitmapFactory.decodeResource(this.resources, R.drawable.level2)
                    3 -> bitmap = BitmapFactory.decodeResource(this.resources, R.drawable.level3)
                    4 -> bitmap = BitmapFactory.decodeResource(this.resources, R.drawable.level4)
                    5 -> bitmap = BitmapFactory.decodeResource(this.resources, R.drawable.level5)
                }
                val bit = Bitmap.createScaledBitmap(bitmap, size, size, false)
                val bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bit)
                marker.setIcon(bitmapDescriptor)
                mMarkerMap[marker.id] = track.TrackID
            }
        }
        progressBar.visibility = View.GONE
    }

    override fun listChasseDbError(throwable: Throwable) {
        Toast.makeText(this, throwable.message, Toast.LENGTH_SHORT).show()
    }

}