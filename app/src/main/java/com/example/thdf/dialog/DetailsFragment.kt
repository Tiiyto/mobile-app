package com.example.thdf.dialog

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.DialogFragment
import androidx.room.Room
import com.example.thdf.*
import com.example.thdf.Maps.ToChasse
import com.example.thdf.dao.*
import com.example.thdf.database.RoomDatabase
import com.example.thdf.presenter.DetailsPresenter
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers


class DetailsFragment: DialogFragment(), DetailsPresenter.DetailsPresenterView {
    val presenter: DetailsPresenter = DetailsPresenter(this)
    private lateinit var database : RoomDatabase
    private lateinit var questionDao: QuestionDao
    private lateinit var progressBar: ProgressBar
    private lateinit var dialog: LoadingDialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val rootView: View = inflater.inflate(
                R.layout.dialog_fragment_details_chasse,
                container,
                false
        )
        dialog = LoadingDialog(context)

        this.progressBar = rootView.findViewById(R.id.progressBar)
        this.progressBar.visibility = View.GONE

        val cancelButton = rootView.findViewById<Button>(R.id.cancelButton)
        val beginButton = rootView.findViewById<Button>(R.id.begin)
        val switchButton = rootView.findViewById<SwitchCompat>(R.id.switch_download)

        val Titre: TextView = rootView.findViewById(R.id.Titre)
        val City: TextView = rootView.findViewById(R.id.City)
        val level: TextView = rootView.findViewById(R.id.level)
        val temps: TextView = rootView.findViewById(R.id.temps)
        val km: TextView = rootView.findViewById(R.id.km)
        val description: TextView = rootView.findViewById(R.id.description)

        Titre.text = CurrentChasseApplication.currentChasse?.NameTrack
        City.text = """${CurrentChasseApplication.currentChasse?.City}, ${CurrentChasseApplication.currentChasse?.Postal}"""
        level.text = CurrentChasseApplication.currentChasse?.Level.toString()
        val time = CurrentChasseApplication.currentChasse?.Track_Time.toString().split(":")
        temps.text = """${time[0]}h${time[1]}"""
        km.text = getString(
                R.string.kilomètre,
                CurrentChasseApplication.currentChasse?.Distance.toString()
        )
        description.text = CurrentChasseApplication.currentChasse?.DescriptionShort
        database = Room.databaseBuilder(context!!, RoomDatabase::class.java, "mydb").build()
        questionDao = database.questionDao()


        cancelButton.setOnClickListener {
            dismiss()
        }
        if (CurrentChasseApplication.currentChasse?.Track_Opened == 0) {
            beginButton.isClickable = false
            beginButton.isEnabled = false
            beginButton.text = getString(R.string.chasse_non_disponible)
            switchButton.visibility = View.GONE
        }
        beginButton.setOnClickListener {
            BeginChasse()
        }

        questionDao.getAllQuestionFromTrack(CurrentChasseApplication.currentChasse!!.TrackID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.isNotEmpty()) {
                        switchButton.isChecked = true
                    }
                }, {
                    Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                })
        switchButton.setOnCheckedChangeListener { buttonView, isChecked ->
            if (buttonView.isPressed) {
                if (isChecked) {
                    Toast.makeText(context, getString(R.string.downloaded_track),Toast.LENGTH_SHORT).show()
                    presenter.downloadAll(context!!)
                } else {
                    presenter.deleteAll(context!!)
                }
            }
        }
        return rootView
    }

    private fun BeginChasse() {
        dialog.show();
        presenter.getAllQuestionFromTrack(context!!)
    }

    override fun getAllQuestionError(throwable: Throwable) {
        Toast.makeText(context, throwable.message, Toast.LENGTH_SHORT).show()
    }
    override fun insertCurrent() {
        val intent = Intent(context!!, ToChasse::class.java)
        startActivity(intent)
        dialog.dismiss()
        dismiss()
    }

    override fun insertCurrentError(throwable: Throwable) {
        dialog.dismiss()
        Toast.makeText(context, throwable.message, Toast.LENGTH_SHORT).show()
    }

    override fun downloadAllError(throwable: Throwable) {
        Log.e("Téléchargement", "Erreur, téléchargement non terminé")
    }
}

