package com.example.thdf.dialog

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import com.example.thdf.R


class LoadingDialog(context: Context?) : ProgressDialog(context) {
    override fun show() {
        super.show()
        setCancelable(false)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setContentView(R.layout.custom_progress_dialog)
    }
}